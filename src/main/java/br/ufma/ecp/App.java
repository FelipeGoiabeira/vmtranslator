package br.ufma.ecp;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * Hello world!
 *
 */
public class App
{


    private static String fromFile(File file) {

        byte[] bytes;
        try {
            bytes = Files.readAllBytes(file.toPath());
            return new String(bytes, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static void translateFile (File file, CodeWriter code) {

        String input = fromFile(file);
        Parser p = new Parser(input);
        while (p.hasMoreCommands()) {
            p.nextCommand().process(code);
        }

    }



    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Please provide a single file path argument.");
            System.exit(1);
        }

        File file = new File(args[0]);

        if (!file.exists()) {
            System.err.println("The file doesn't exist.");
            System.exit(1);
        }

        // we need to compile every file in the directory
        if (file.isDirectory()) {

            var outputFileName = file.getAbsolutePath() +"/"+ file.getName()+".asm";
            System.out.println(outputFileName);
            CodeWriter code = new CodeWriter(outputFileName);

            //code.writeInit();

            for (File f : file.listFiles()) {
                if (f.isFile() && f.getName().endsWith(".vm")) {

                    var inputFileName = f.getAbsolutePath();
                    var pos = inputFileName.indexOf('.');


                    System.out.println("compiling " +  inputFileName);
                    translateFile(f,code);

                }

            }
            code.save();
            // we only compile the single file
        } else if (file.isFile()) {
            if (!file.getName().endsWith(".vm"))  {
                System.err.println("Please provide a file name ending with .vm");
                System.exit(1);
            } else {
                var inputFileName = file.getAbsolutePath();
                var pos = inputFileName.indexOf('.');
                var outputFileName = inputFileName.substring(0, pos) + ".asm";
                CodeWriter code = new CodeWriter(outputFileName);
                System.out.println("compiling " +  inputFileName);
                //code.writeInit();
                translateFile(file,code);
                code.save();
            }
        }
    }


}
