package br.ufma.ecp;

import java.util.ArrayList;
import java.util.List;

public class Command {

    public enum Type {
        ADD(CodeWriter.writeArithmeticAdd()),
        SUB(CodeWriter.writeArithmeticSub()),
        NEG(CodeWriter.writeArithmeticNeg()),
        EQ(CodeWriter.writeArithmeticEq()),
        GT(CodeWriter.writeArithmeticGt()),
        LT(CodeWriter.writeArithmeticLt()),
        AND(CodeWriter.writeArithmeticAnd()),
        OR(CodeWriter.writeArithmeticOr()),
        NOT(CodeWriter.writeArithmeticNot()),
        PUSH(CodeWriter.writePush()),
        POP(CodeWriter.writePop()),
        LABEL(CodeWriter.defaultProcessor()),
        GOTO(CodeWriter.defaultProcessor()),
        IF(CodeWriter.defaultProcessor()),
        RETURN(CodeWriter.defaultProcessor()),
        CALL(CodeWriter.defaultProcessor()),
        FUNCTION(CodeWriter.defaultProcessor());

        CodeWriter.CommandProcessor commandProcessor;

        Type(CodeWriter.CommandProcessor commandProcessor) {
            this.commandProcessor = commandProcessor;
        }
    }

    public Command.Type type;
    public List<String> args = new ArrayList<>();

    public Command (String[] command) {

        if (command[0].equals("if-goto"))
            type = Type.IF;
        else
            type = Command.Type.valueOf(command[0].toUpperCase());

        for (int i=1;i<command.length;i++){
            var arg = command[i];
            var pos = arg.indexOf("//");
            if (pos != -1) arg = arg.substring(0, pos);
            args.add(arg.strip());
        } 
    }

    public void process(CodeWriter codeWriter){
        type.commandProcessor.process(codeWriter, this);
    }
    
}
