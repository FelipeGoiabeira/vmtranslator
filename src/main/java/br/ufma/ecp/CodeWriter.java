package br.ufma.ecp;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class CodeWriter {

    private StringBuilder output = new StringBuilder();
    public String moduleName = "Main";
    public int labelCount = 0;
    public String outputFileName;
    public int callCount = 0;
    public int returnSubCount;

    interface CommandProcessor {
        void process(CodeWriter codeWriter, Command command);
    }

    public CodeWriter(String fname) {
        outputFileName = fname;
    }

    void setFileName(String s) {
        moduleName = s.substring(0, s.indexOf("."));
        moduleName = moduleName.substring(s.lastIndexOf("/") + 1);
        System.out.println(moduleName);
    }

    String registerName(String segment, int index) {

        if (segment.equals("local"))
            return "LCL";
        if (segment.equals("argument"))
            return "ARG";
        if (segment.equals("this"))
            return "THIS";
        if (segment.equals("that"))
            return "THAT";
        if (segment.equals("pointer"))
            return "R" + (3 + index);
        if (segment.equals("temp"))
            return "R" + (5 + index);

        return moduleName + "." + index;
    }

    public void writeInit() {
        write("@256");
        write("D=A");
        write("@SP");
        write("M=D");
        writeCall("Sys.init", 0);
    }

    void writeCall(String funcName, int numArgs) {

        /*
           push return-address     // (using the label declared below)
           push LCL                // save LCL of the calling function
           push ARG                // save ARG of the calling function
           push THIS               // save THIS of the calling function
           push THAT               // save THAT of the calling function
           ARG = SP-n-5            // reposition ARG (n = number of args)
           LCL = SP                // reposiiton LCL
           goto f                  // transfer control
           (return-address)        // declare a label for the return-address
        */

        var comment = String.format("// call %s %d", funcName, numArgs);

        var returnAddr = String.format("%s_RETURN_%d", funcName, callCount);
        callCount++;

        write(String.format("@%s %s", returnAddr, comment)); // push return-addr
        write("D=A");
        write("@SP");
        write("A=M");
        write("M=D");
        write("@SP");
        write("M=M+1");
        /*
        writeFramePush("LCL");
        writeFramePush("ARG");
        writeFramePush("THIS");
        writeFramePush("THAT");
         */
        var returnFrame = String.format("$RET%d", returnSubCount);
        write(String.format("@%s", returnFrame));
        write("D=A");
        write("@$FRAME$");
        write("0;JMP");
        write(String.format("(%s)", returnFrame));
        returnSubCount++;



        write(String.format("@%d", numArgs)); // ARG = SP-n-5
        write("D=A");
        write("@5");
        write("D=D+A");
        write("@SP");
        write("D=M-D");
        write("@ARG");
        write("M=D");

        write("@SP");// LCL = SP
        write("D=M");
        write("@LCL");
        write("M=D");

        writeGoto(funcName);

        write("(" + returnAddr + ")"); // (return-address)

    }


    void writeFramePush(String value) {
        write("@" + value);
        write("D=M");
        write("@SP");
        write("A=M");
        write("M=D");
        write("@SP");
        write("M=M+1");
    }

    public static CommandProcessor defaultProcessor() {
        return (codeWriter, command) -> {
             System.out.println(command.type.toString()+" not implemented");
        };
    }


    public static CommandProcessor writePush() {
        return (codeWriter, command) -> {
            var seg = command.args.get(0);
            var index = Integer.parseInt(command.args.get(1));

            if (seg.equals("constant")) {
                codeWriter.write("@" + index + " // push " + seg + " " + index);
                codeWriter.write("D=A");
                codeWriter.write("@SP");
                codeWriter.write("A=M");
                codeWriter.write("M=D");
                codeWriter.write("@SP");
                codeWriter.write("M=M+1");
            } else if (seg.equals("static") || seg.equals("temp") || seg.equals("pointer")) {
                codeWriter.write("@" + codeWriter.registerName(seg, index) + " // push " + seg + " " + index);
                codeWriter.write("D=M");
                codeWriter.write("@SP");
                codeWriter.write("A=M");
                codeWriter.write("M=D");
                codeWriter.write("@SP");
                codeWriter.write("M=M+1");
            } else {
                codeWriter.write("@" + codeWriter.registerName(seg, 0) + " // push " + seg + " " + index);
                codeWriter.write("D=M");
                codeWriter.write("@" + index);
                codeWriter.write("A=D+A");
                codeWriter.write("D=M");
                codeWriter.write("@SP");
                codeWriter.write("A=M");
                codeWriter.write("M=D");
                codeWriter.write("@SP");
                codeWriter.write("M=M+1");
            }
        };
    }

    public static CommandProcessor writePop() {
        return (codeWriter, command) -> {
            var seg = command.args.get(0);
            var index = Integer.parseInt(command.args.get(1));

            if (seg.equals("static") || seg.equals("temp") || seg.equals("pointer")) {
                codeWriter.write("@SP // pop " + seg + " " + index);
                codeWriter.write("M=M-1");
                codeWriter.write("A=M");
                codeWriter.write("D=M");
                codeWriter.write("@" + codeWriter.registerName(seg, index));
                codeWriter.write("M=D");
            } else {
                codeWriter.write("@" + codeWriter.registerName(seg, 0) + " // pop " + seg + " " + index);
                codeWriter.write("D=M");
                codeWriter.write("@" + index);
                codeWriter.write("D=D+A");
                codeWriter.write("@R13");
                codeWriter.write("M=D");
                codeWriter.write("@SP");
                codeWriter.write("M=M-1");
                codeWriter.write("A=M");
                codeWriter.write("D=M");
                codeWriter.write("@R13");
                codeWriter.write("A=M");
                codeWriter.write("M=D");
            }
        };
    }

    public static CommandProcessor writeArithmeticAdd() {
        return (codeWriter, command) -> {
            codeWriter.write("@SP // add");
            codeWriter.write("M=M-1");
            codeWriter.write("A=M");
            codeWriter.write("D=M");
            codeWriter.write("A=A-1");
            codeWriter.write("M=D+M");
        };
    }

    public static CommandProcessor writeArithmeticSub() {
        return (codeWriter, command) -> {
            codeWriter.write("@SP // sub");
            codeWriter.write("M=M-1");
            codeWriter.write("A=M");
            codeWriter.write("D=M");
            codeWriter.write("A=A-1");
            codeWriter.write("M=M-D");
        };
    }

    public static CommandProcessor writeArithmeticNeg() {
        return (codeWriter, command) -> {
            codeWriter.write("@SP // neg");
            codeWriter.write("A=M");
            codeWriter.write("A=A-1");
            codeWriter.write("M=-M");
        };
    }

    public static CommandProcessor writeArithmeticAnd() {
        return (codeWriter, command) -> {
            codeWriter.write("@SP // and");
            codeWriter.write("AM=M-1");
            codeWriter.write("D=M");
            codeWriter.write("A=A-1");
            codeWriter.write("M=D&M");
        };
    }

    public static CommandProcessor writeArithmeticOr() {
        return (codeWriter, command) -> {
            codeWriter.write("@SP // or");
            codeWriter.write("AM=M-1");
            codeWriter.write("D=M");
            codeWriter.write("A=A-1");
            codeWriter.write("M=D|M");
        };
    }

    public static CommandProcessor writeArithmeticNot() {
       return (codeWriter, command) -> {
           codeWriter.write("@SP // not");
           codeWriter.write("A=M");
           codeWriter.write("A=A-1");
           codeWriter.write("M=!M");
       };
    }

    public static CommandProcessor writeArithmeticEq() {
        return (codeWriter, command) -> {
            String label = ("JEQ_" + codeWriter.moduleName + "_" + (codeWriter.labelCount));
            codeWriter.write("@SP // eq");
            codeWriter.write("AM=M-1");
            codeWriter.write("D=M");
            codeWriter.write("@SP");
            codeWriter.write("AM=M-1");
            codeWriter.write("D=M-D");
            codeWriter.write("@" + label);
            codeWriter.write("D;JEQ");
            codeWriter.write("D=1");
            codeWriter.write("(" + label + ")");
            codeWriter.write("D=D-1");
            codeWriter.write("@SP");
            codeWriter.write("A=M");
            codeWriter.write("M=D");
            codeWriter.write("@SP");
            codeWriter.write("M=M+1");

            codeWriter.labelCount++;
        };
    }

    public static CommandProcessor writeArithmeticGt() {
        return (codeWriter, command) -> {
            String labelTrue = ("JGT_TRUE_" + codeWriter.moduleName + "_" + (codeWriter.labelCount));
            String labelFalse = ("JGT_FALSE_" + codeWriter.moduleName + "_" + (codeWriter.labelCount));

            codeWriter.write("@SP // gt");
            codeWriter.write("AM=M-1");
            codeWriter.write("D=M");
            codeWriter.write("@SP");
            codeWriter.write("AM=M-1");
            codeWriter.write("D=M-D");
            codeWriter.write("@" + labelTrue);
            codeWriter.write("D;JGT");
            codeWriter.write("D=0");
            codeWriter.write("@" + labelFalse);
            codeWriter.write("0;JMP");
            codeWriter.write("(" + labelTrue + ")");
            codeWriter.write("D=-1");
            codeWriter.write("(" + labelFalse + ")");
            codeWriter.write("@SP");
            codeWriter.write("A=M");
            codeWriter.write("M=D");
            codeWriter.write("@SP");
            codeWriter.write("M=M+1");

            codeWriter.labelCount++;
        };
    }

    public static CommandProcessor writeArithmeticLt() {
        return (codeWriter, command) -> {
            String labelTrue = ("JLT_TRUE_" + codeWriter.moduleName + "_" + (codeWriter.labelCount)); // toDo ; module
            String labelFalse = ("JLT_FALSE_" + codeWriter.moduleName + "_" + (codeWriter.labelCount));

            codeWriter.write("@SP // lt");
            codeWriter.write("AM=M-1");
            codeWriter.write("D=M");
            codeWriter.write("@SP");
            codeWriter.write("AM=M-1");
            codeWriter.write("D=M-D");
            codeWriter.write("@" + labelTrue);
            codeWriter.write("D;JLT");
            codeWriter.write("D=0");
            codeWriter.write("@" + labelFalse);
            codeWriter.write("0;JMP");
            codeWriter.write("(" + labelTrue + ")");
            codeWriter.write("D=-1");
            codeWriter.write("(" + labelFalse + ")");
            codeWriter.write("@SP");
            codeWriter.write("A=M");
            codeWriter.write("M=D");
            codeWriter.write("@SP");
            codeWriter.write("M=M+1");

            codeWriter.labelCount++;
        };
    }

    void writeLabel(String label) {
        write("(" + label + ")");
    }

    void writeGoto(String label) {
        write("@" + label);
        write("0;JMP");
    }

    private void write(String s) {
        output.append(String.format("%s\n", s));
    }

    public String codeOutput() {
        return output.toString();
    }

    public void save() {

        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(outputFileName);

            outputStream.write(output.toString().getBytes());

            outputStream.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}

